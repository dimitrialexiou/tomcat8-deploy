## Synopsis

With the usage of Amazon's CloudFormation and Ansible, we will deploy a clojure-collector to a tomcat instance. With the CloudFormation .json file we will create a multi-az, load balanced and auto scaled Apache Tomcat instance. The application is configured to span all availability zones in the region and is auto-scaled based on the CPU utilization of the Apache Tomcat servers. Notifications will be sent to the operator email address on scaling events. The instance(s) are load balanced with a simple health check against the default web page.

## Installation

Once you are logged in on your AWS console please create a new key pair if you already don't have one. When the key pair is ready you will need to go under the CloudFormation service. 

Create a new stack and select the stack-clojure-collector.txt file that is located in this repository, then click on next. On the second page, give the the stack we create, a name. The instance type is by default set to t2.micro. Select the key name you have created earlier and enter the email address you want to receive the alerts when auto-scaling is taking place. In the SSH location you can assign and IP address if you want to specifically SSH to the instance only from one IP. Click next and specify tags (key-value pairs) for resources in your stack. Again hit on next review and create the stack. 

The creation of the stack will take some time (give it 5 minutes). When the status of the stack is CREATE_COMPLETE go at the outputs tab at the stack you created under CloudFormation. You will see a URL like like this: http://gousto-st-ElasticL-135AKVZH8WOFM-1997345159.us-west-2.elb.amazonaws.com:8080/clojure-collector-1.1.0-standalone/i

Click on it and you are ready to go. Now we have an instance running the clojure-collector in an Apache Tomcat 8 behind a load-balancer. If the CPU load goes > 70% for 10 minutes then the auto-scaling event is triggered and two more instances are span to all availability zones in the region.